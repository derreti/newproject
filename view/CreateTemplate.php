<?php
$timestamp = date('Y-m-d H:i:s', time());
?>

<form method="post" action="../controller/create.php">
    <input type="text" required name="name" placeholder="name">
    <br>
    <input type="text" required name="description" placeholder="description">
    <br>
    <input type="text" required name="created_at" placeholder="created_at" value="<?php echo $timestamp ?>">
    <br>
    <input type="submit">
</form>
