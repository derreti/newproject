<?php


class Article
{
    private function getConnection()
    {
        $pdoConnection = new PDO('pgsql:host=127.0.0.1;dbname=postgres', 'postgres', 'al200307');
        return $pdoConnection;
    }
    public function insert($name, $description, $created_at)
    {
        $pdoConnection = $this->getConnection();

        $sql = 'INSERT INTO article (name, description, created_at) VALUES (:name, :description, :created_at)';
        $statement = $pdoConnection->prepare($sql);
        $statement -> bindValue(':name', $name);
        $statement -> bindValue(':description', $description);
        $statement -> bindValue(':created_at', $created_at);
        $statement -> execute();
        $statement -> errorInfo();
    }
    public function findAll(){

        $pdoConnection = $this->getConnection();

        $sql = 'SELECT * FROM article';

        $statement = $pdoConnection->prepare($sql);
        $statement -> execute();
        return $statement->fetchAll();
    }
    public function update($id, $name, $description, $created_at){

        $pdoConnection = $this->getConnection();

        $sql = 'UPDATE article SET name = :name, description = :description, created_at = :created_at WHERE id = :id ';
        $pdoStatement = $pdoConnection->prepare($sql);
        $pdoStatement->bindValue(':name', $name);
        $pdoStatement->bindValue(':description', $description);
        $pdoStatement->bindValue(':created_at', $created_at);
        $pdoStatement->bindValue(':id', $id);
        $result = $pdoStatement->execute();
//        var_dump($pdoStatement->errorInfo());
//        exit;
        return $result;
    }
    public function findById($id){
        $pdoConnection = $this->getConnection();
        $sql = 'SELECT * FROM article WHERE id = :id';
        $pdoStatement = $pdoConnection->prepare($sql);
        $pdoStatement->bindValue(':id', $id);
        $pdoStatement->execute();
        return $pdoStatement->fetch();
    }
    public function deleteById($id){
        $pdoConnection = $this->getConnection();
        $sql = 'DELETE FROM article WHERE id = :id';
        $pdoStatement = $pdoConnection->prepare($sql);
        $pdoStatement->bindValue(':id', $id);
        return $pdoStatement->execute();

    }
}
