<?php
include_once __DIR__ . "/../model/Article.php";
date_default_timezone_set("Europe/Kiev");
if (isset($_POST['name']) && isset($_POST['description']) && isset($_POST['created_at'])) {
    $article = new article();
    $article->update($_GET['id'], $_POST['name'], $_POST['description'], $_POST['created_at']);
    header('Location: ../index.php');
}
if (isset($_GET['id'])) {
    $articles = new article();
    $row = $articles->findById($_GET['id']);
} else {
    header('Location: ../index.php');
}

require_once __DIR__."/../view/EditTemplate.php";